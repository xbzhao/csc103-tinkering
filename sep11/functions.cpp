// functions.
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;

/* Functions give us yet another way to control the flow of execution of the
 * program. main() is a function that we've seen over and over... */

/* syntax:
 * datatype functionname(parameterlist) { c++ statements...}
 * think of f(x) from your calculus days... remember this notation?
 * f:R ---> R
 * Breaking it down, it says there is a function named f, which has domain R
 * and range R.  The C++ equivalent would be something like:
 * double f(double);
 * NOTE: thing above is called a "function prototype"
 */

// example: a function to compute x^4
double f(double x) {
	return x*x*x*x;
	// the return statement ends the function, and it
	// reports the RHS as the value for f(x).
}

/* functions can call other functions.  They can even call themselves!
 * (We'll see more of that later on when we study recursion...)
 * Here's an example: use our function f above to compute the sum of 4th powers
 * of consecutive integers: 1^4 + 2^4 + ... x^4 */

// Notation: k is the "formal parameter"
double sum4thpowers(int k)
{
	// by Rufina :D
	double sum;
	sum=0;
	for(int n=1; n<=k; n++)
	{
		sum=sum+f(n);
	}
	return sum;
}

int testFunction(int k) // parameter k is "call by value"
{
	k = 22;
	return k;
}

int testFunctionbyReference(int& k) // parameter k is "call by reference"
{
	k = 22;
	return k;
}

// TODO: write a function that takes 3 integers and returns maximal value

// TODO: write a function that takes 3 doubles and returns the average value

// TODO: finish your homework, and convert that into a function called isPrime,
// which takes an integer and returns a boolean value indicating whether or not
// it is prime.

// TODO: write a function that takes two integer parameters and swaps the
// contents, i.e., if x=2 and y=5, then after calling swap(x,y), y=2 and x=5

int main()
{
	// how to call functions? Use the familiar f(x) notation...
	double x = 13;
	cout << f(x) << endl;
	int numTerms;
	cout << "enter the number of terms: ";
	cin >> numTerms;
	cout << sum4thpowers(numTerms) << endl;
	// NOTE: numTerms is called the "actual parameter".
	numTerms = 42;
	testFunction(numTerms);
	// will this change numTerms??
	cout << "numTerms == " << numTerms << endl;
	// NO!  Functions default to "call byt value" in C++
	// this means that the formal parameter is a COPY of the actual parameter.
	// The alternative is "call by reference":
	testFunctionbyReference(numTerms);
	cout << "numTerms == " << numTerms << endl;
	return 0;
}
