#include <iostream> // we're going to re-use code from the iostream library,
					// which provides functionality for basic input / output.
					// this saves us from re-inventing the wheel.
#include <string>
// NOTE: lines that start with "//" are not a part of the program. Terminology:
// "comments".
/* There are also multiline comments,
 * which begin with the slash-star,
 * and end with star-slash. */

// Okay- we need to specify where the program starts.  In C++, it is usually a
// function called "main", which looks something like this:
int main() {
	// main signifies where the program should begin execution.
	// lines after this are where your program starts...
	// Let's try something simple -- just printing a message to the screen:
	std::cout << "Hello world.\n\n";
	// cout came from iostream.  This is functionality we're importing, so we
	// don't have to write code that handles printing.
	// the "std::" is the *namespace* that cout belongs to.
	// think of it like the last name of cout: bill Jones --> Jones::bill.
	// for the moment, just think of "<<" as an abstract binary operation.
	// It will have the side effect of printing out the RHS to the screen.
	// the stuff inside quotes (hello world) is a *string literal*.  Quotes are
	// needed to differentiate a literal from an *identifier* (variables,
	// typenames, etc.).
	// TODO: delete the quotes, try to compile, see what happens.
	// The "\n" is the newline character.  This is known as an *escape
	// sequence*.
	// TODO: what happens if we replace the \n with an actual newline?  Try it
	// out.


	/********* variables *********/
	// variables are convenient names for some piece of memory on the machine.
	int x;  // this declares a variable named x of type integer.
	// NOTE: every variable in C++ has a type.  This is important!!!
	// Specifying the type allows the compiler to figure out how much space to
	// allocate, but types play a bigger role than just that.  More on this
	// later on.
	// So, how do we read and write x?  To read, just do:
	x = 20; /* The equals sign serves as the assignment operator.  This
			   statement will put 20 into x. */
	// Note the semicolon!  This is how statements are delimited in C++
	// TODO: delete the semicolon, try to compile, see what happens.
	int y;
	y = x; // set y equal to x.
	y = 234; // set y equal to 234.
	// how would we switch the contents of y and x?
	// x = y; y = x; // <--- fail. x_x  This works though:
	int z = x;
	x = y;
	y = z;
	// TODO: (for people with prior C/C++) how to do this without z?

	// last thing before we go:  how to read input from the user?  We again
	// invoke functions from iostream:
	// read an integer:
	std::cout << "Enter an integer: ";
	std::cin >> z;  // reads an integer from standard input.
	// how to deal with character strings?  Yet again, we will invoke a
	// library.  this time, the <string> library.
	std::cout << "Enter a string: ";
	std::string s;
	std::cin >> s; // reads a character string into s.
	// now echo it back out:
	std::cout << s;
	// NOTE: every variable you declare in C++ has a *datatype*.
	// The compiler has to know what kind of data it is working with when you
	// refer to your variables.

	return 0; // return will leave main, thus ending your program.
}
